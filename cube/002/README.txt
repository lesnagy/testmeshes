This directory contains a tecplot file for a finite element mesh model of a 
cuboctahedron. 

Details for the cuboctahedron are
  Vertices: 7368
  Elements: 38481

Note: this tecplot file contains a field associated with the mesh.

Contents:
  Temp_random7_mult.cpp  A C++ file version of the information held
                         in 'Temp_random7_mult.dat'.

  Temp_random7_mult.dat  The tecplot data file containing mesh vertices, 
                         magnetisation and topology information.

  Temp_random7.png       An image of the geometry.

  Temp_random7_mag.png   An image of the magnetisation.
