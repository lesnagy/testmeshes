This directory contains patran and exodus files for a finite element mesh model 
of a cube (for testing purposes). This is the "classic" 80nm cube which should
result in a vortex core after minimization in a zero field from saturation.

Details for the cube are
  Vertices: 2589
  Elements: 12930

Contents:
  
  cubeMesh003.cubit The cubit/trelis file used to generate the mesh.

  cubeMesh003.e     The 80nm cube mesh in exodusII file format.

  cubeMesh003.pat   The 80nm cube mesh in patran file format.

  result.dat        The tecplot output of the energy minimisation after running
                    the script "script.txt".

  script.txt        Merrill script, minimises the structure for an 80nm cube of 
                    magnetite from a saturated state (along <1,1,1> direction) 
                    in zero field.

  mag_and_volume.png The soution after minimisation, i.e. after Merrill is run
                     with input script "script.txt", showing both the volume
                     and the magnetisation.

  mag_only.png       The soution after minimisation, i.e. after Merrill is run
                     with input script "script.txt", showing both the 
                     magnetisation only.

  volume_only.png    An image of the mesh showing element sizes.
