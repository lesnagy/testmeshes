This directory contains patran and exodus files for a finite element mesh model 
of a cube (for testing purposes). 

Details for the cube are
  Vertices: 309
  Elements: 1261

These patran files do *NOT* contain a field associated with the mesh.
